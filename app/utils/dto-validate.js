module.exports = (contexto, schema) => {
    return async(req, res, next) => {
        const result = schema.validate(req[contexto], { abortEarly: false });
        if (result.error){
            return res.render("pages/errors", {
                errors: result.error.details.map(item => {return item.message})
            })
        }
        next()
    }
}