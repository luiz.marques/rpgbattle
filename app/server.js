const express = require('express');
const path = require('path');
const routes = require('./routes/routes');
const bp = require('body-parser');

const server = express();
server.use(bp.json());
server.use(bp.urlencoded());
server.set("view engine", "ejs");
server.set("views", path.join(__dirname, "views"));
server.use(express.static(path.join(__dirname, "public")));

routes(server);

server.listen(process.env.PORT || 3000, () => {})