const formController= require("../../controllers/formController");
const validateDTO = require("../../utils/dto-validate");

module.exports = (routev1) => {
    routev1
    .route("/form")
    .get(formController.getForm)
    .post(validateDTO("body", formController.postFormSchema), formController.postForm)
}