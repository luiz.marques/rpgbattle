const aboutController = require("../../controllers/aboutController");

module.exports = (routev1) => {
    routev1.route("/about")
    .get((req, res, next) => {
        next()
    }, aboutController.metodoControllerAbout
    )
}