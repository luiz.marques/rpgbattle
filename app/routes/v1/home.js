const homeController = require("../../controllers/homeController");

module.exports = (routev1) => {
    routev1.route("/")
    .get((req, res, next) => {
        next()
    }, homeController.metodoControllerHome
    )
}