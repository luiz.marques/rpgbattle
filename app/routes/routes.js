const express = require("express");
const v1Home = require("./v1/home");
const v1Form = require("./v1/form");
const v1About = require("./v1/about");

module.exports = (app) =>{
    const routesV1 = express.Router();
    v1Home(routesV1)
    v1Form(routesV1)
    v1About(routesV1)
    app.use('', routesV1)
}
