const racesDB = [
    {
        id: 1, 
        name: "Human",
        url: "https://i.imgur.com/LEXJxsP.jpeg",
    },
    { 
        id: 2, 
        name: "Dawrf",
        url: "https://i.imgur.com/GnDk5qx.jpeg",
    },
    { 
        id: 3, 
        name: "Elf",
        url: "https://pcmedia.ign.com/pc/image/article/686/686628/the-lord-of-the-rings-the-battle-for-middle-earth-ii-20060207041453970.jpg",
    },
    {
        id: 4, 
        name: "Hobbit",
        url: "https://i.imgur.com/cyOmn1o.jpeg",
    },
];

function changeAvatar(){
    const raceOption = document.getElementById("race").value
    
    const img = racesDB.find(item => item.id == raceOption)
    document.getElementById("avatar").src = img.url
}