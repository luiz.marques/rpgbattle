const professionsDB = [
    {
        id: 1, 
        name: "Fighter",
    },
    { 
        id: 2, 
        name: "Mage",
    },
    { 
        id: 3, 
        name: "Ranger",
    },
];


const listAll = () => {

    return professionsDB;

}

const searchById = (id) => {

    const result = professionsDB.filter((item) => {
           return parseInt(item.id) === parseInt(id);
    });

    return result.length > 0 ? result[0] : undefined;

}


const searchByName = (id) => {

    const result = professionsDB.filter((item) => {
           return parseInt(item.id) === parseInt(id);
    });

    return result.length > 0 ? result[0] : undefined;

}

const createNewProfession = ({ id, name }) => {

    // const result = sexoDB.filter((item) => {
    //     return item.id === id;
    // });

    //todo: verificando se o id ja existe
    
    //todo: verificando se a descricao ja existe



    professionsDB.push({ id, name });

}


module.exports = {
    listAll,
    searchById,
    searchByName,
    professionsDB,
}