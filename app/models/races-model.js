const racesDB = [
    {
        id: 1, 
        name: "Human",
        url: "https://i.imgur.com/LEXJxsP.jpeg",
    },
    { 
        id: 2, 
        name: "Dawrf",
        url: "https://i.imgur.com/GnDk5qx.jpeg",
    },
    { 
        id: 3, 
        name: "Elf",
        url: "https://pcmedia.ign.com/pc/image/article/686/686628/the-lord-of-the-rings-the-battle-for-middle-earth-ii-20060207041453970.jpg",
    },
    {
        id: 4, 
        name: "Hobbit",
        url: "https://i.imgur.com/cyOmn1o.jpeg",
    },
];


const listAll = () => {

    return racesDB;

}

const searchById = (id) => {

    const result = racesDB.filter((item) => {
           return parseInt(item.id) === parseInt(id);
    });

    return result.length > 0 ? result[0] : undefined;

}

const searchByDescription = (id) => {

    const result = racesDB.filter((item) => {
           return parseInt(item.id) === parseInt(id);
    });

    return result.length > 0 ? result[0] : undefined;

}


const createNewRace = ({ id, name }) => {

    // const result = sexoDB.filter((item) => {
    //     return item.id === id;
    // });

    //todo: verificando se o id ja existe
    
    //todo: verificando se a descricao ja existe



    racesDB.push({ id, name });

}


module.exports = {
    listAll,
    searchById,
    createNewRace,
    racesDB,
}
