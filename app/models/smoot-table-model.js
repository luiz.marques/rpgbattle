const smootTableDB = [
    {id: 1, bonus: -10,},
    {id: 2,bonus: -10,},
    {id: 3,bonus: -9,},
    {id: 4,bonus: -9,},
    {id: 5,bonus: -8,},
    {id: 6,bonus: -8,},
    {id: 7,bonus: -7,},
    {id: 8,bonus: -7,},
    {id: 9,bonus: -6,},
    {id: 10,bonus: -5,},
    {id: 11,bonus: -4,},
    {id: 12,bonus: -4,},
    {id: 13,bonus: -4,},
    {id: 14,bonus: -4,},
    {id: 15,bonus: -4,},
    {id: 16,bonus: -3,},
    {id: 17,bonus: -3,},
    {id: 18,bonus: -3,},
    {id: 19,bonus: -3,},
    {id: 20,bonus: -3,},
    {id: 21,bonus: -2,},
    {id: 22,bonus: -2,},
    {id: 23,bonus: -2,},
    {id: 24,bonus: -2,},
    {id: 25,bonus: -2,},
    {id: 26,bonus: -1,},
    {id: 27,bonus: -1,},
    {id: 28,bonus: -1,},
    {id: 29,bonus: -1,},
    {id: 31,bonus: 0,},
    {id: 32,bonus: 0,},
    {id: 33,bonus: 0,},
    {id: 34,bonus: 0,},
    {id: 35,bonus: 0,},
    {id: 36,bonus: 0,},
    {id: 37,bonus: 0,},
    {id: 38,bonus: 0,},
    {id: 39,bonus: 0,},
    {id: 40,bonus: 0,},
    {id: 41,bonus: 0,},
    {id: 42,bonus: 0,},
    {id: 43,bonus: 0,},
    {id: 44,bonus: 0,},
    {id: 45,bonus: 0,},
    {id: 46,bonus: 0,},
    {id: 47,bonus: 0,},
    {id: 48,bonus: 0,},
    {id: 49,bonus: 0,},
    {id: 50,bonus: 0,},
    {id: 51,bonus: 0,},
    {id: 52,bonus: 0,},
    {id: 53,bonus: 0,},
    {id: 54,bonus: 0,},
    {id: 55,bonus: 0,},
    {id: 56,bonus: 0,},
    {id: 57,bonus: 0,},
    {id: 58,bonus: 0,},
    {id: 59,bonus: 0,},
    {id: 60,bonus: 0,},
    {id: 61,bonus: 0,},
    {id: 62,bonus: 0,},
    {id: 63,bonus: 0,},
    {id: 64,bonus: 0,},
    {id: 65,bonus: 0,},
    {id: 66,bonus: 0,},
    {id: 67,bonus: 0,},
    {id: 68,bonus: 0,},
    {id: 69,bonus: 0,},
    {id: 70,bonus: 1,},
    {id: 71,bonus: 1,},
    {id: 72,bonus: 1,},
    {id: 73,bonus: 1,},
    {id: 74,bonus: 1,},
    {id: 75,bonus: 2,},
    {id: 76,bonus: 2,},
    {id: 77,bonus: 2,},
    {id: 78,bonus: 2,},
    {id: 79,bonus: 2,},
    {id: 81,bonus: 3,},
    {id: 82,bonus: 3,},
    {id: 83,bonus: 3,},
    {id: 84,bonus: 3,},
    {id: 85,bonus: 3,},
    {id: 86,bonus: 4,},
    {id: 87,bonus: 4,},
    {id: 88,bonus: 4,},
    {id: 89,bonus: 4,},
    {id: 90,bonus: 5,},
    {id: 91,bonus: 5,},
    {id: 92,bonus: 6,},
    {id: 93,bonus: 6,},
    {id: 94,bonus: 7,},
    {id: 95,bonus: 7,},
    {id: 96,bonus: 8,},
    {id: 97,bonus: 8,},
    {id: 98,bonus: 9,},
    {id: 99,bonus: 9,},
    {id: 100,bonus: 10,},
    {id: 101,bonus: 12,},
    {id: 102,bonus: 14,},
];

const listAll = () => {

    return smootTableDB;

}

const searchById = (id) => {

    const result = smootTableDB.filter((item) => {
           return parseInt(item.id) === parseInt(id);
    });

    return result.length > 0 ? result[0] : undefined;

}


const searchByDescription = (id) => {

    const result = smootTableDB.filter((item) => {
           return parseInt(item.id) === parseInt(id);
    });

    return result.length > 0 ? result[0] : undefined;

}

module.exports = {
    listAll,
    searchById,
    smootTableDB
}