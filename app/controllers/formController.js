const racesModel = require("../models/races-model");
const professionsModel = require("../models/professions-model");

// const smootTableModel = require("../models/smoot-table-model");


const fs = require("fs");
const Joi = require("joi");
const ejs = require("ejs");
const htmlToPdf = require("html-pdf-node");
const path = require("path");
const { join } = require("path");

const racesDB = racesModel.racesDB;
const professionsDB = professionsModel.professionsDB;
// const smootTableDB = smootTableModel.smootTableDB;

const viewModel = {
    racesDB,
    professionsDB
    // smootTableDB
}

const getForm = (req, res, next) => {
    res.render("pages/form", viewModel)
    // console.log(viewModel);
}

const postForm = (req, res, next) => {
  const {avatar, name, race, profession, age, lvl,
          st, ag, co, ig, it,
          stBonus, agBonus, coBonus, igBonus, itBonus,
          bonus0, bonus1, bonus2,bonus3,bonus4,
          bonus00, bonus01, bonus02,bonus03,bonus04,
          bonus000, bonus001,bonus002,bonus003, bonus004,
          movimentsRemains, weaponsRemains
      } = req.body

    const selectedRace = racesModel.searchById(race);
    const selectedProfessions = professionsModel.searchById(profession);
   
    const pdfViewModel = {
      name, race: selectedRace.name, avatar: selectedRace.url, profession: selectedProfessions.name, 
      age, lvl,
      st, ag, co, ig, it,
      stBonus, agBonus, coBonus, igBonus, itBonus,
      bonus0, bonus1, bonus2, bonus3, bonus4,
      bonus00, bonus01, bonus02, bonus03, bonus04,
      bonus000, bonus001, bonus002, bonus003, bonus004,
      movimentsRemains, weaponsRemains
      
    };

    //TODO: montar o html
    var filePath = path.join(__dirname, "../views/components/form-pdf.ejs");
    var templateHtml = fs.readFileSync(filePath, 'utf8');
    
    //TODO: montar o pdf
    const htmlPronto = ejs.render(templateHtml, pdfViewModel);
  
    //TODO: retornar o pdf
  
    const file = {
      content: htmlPronto  
    };
  
    const configuracoes = {
      format: 'A4',
      printBackground: true
    };
  
    htmlToPdf.generatePdf(file, configuracoes)
    .then((resultPromessa) => {
        res.contentType("application/pdf");
        res.send(resultPromessa);
      });
  };
  

const postFormSchema = Joi.object({
    name: Joi.string().max(30).min(5).required(),
    age:  Joi.number(),
    race: Joi.number(),
    lvl: Joi.number(),
    profession: Joi.number(),
    st: Joi.number(), 
    ag: Joi.number(), 
    co: Joi.number(),
    ig: Joi.number(),
    it: Joi.number(),
    stBonus: Joi.number(),
    agBonus: Joi.number(),
    coBonus: Joi.number(),
    igBonus: Joi.number(),
    itBonus: Joi.number(),
    bonus0: Joi.number(),
    bonus1: Joi.number(),
    bonus2: Joi.number(),
    bonus3: Joi.number(),
    bonus4: Joi.number(),
    bonus00: Joi.number(),
    bonus01: Joi.number(),
    bonus02: Joi.number(),
    bonus03: Joi.number(),
    bonus04: Joi.number(),
    bonus000: Joi.number(),
    bonus001: Joi.number(),
    bonus002: Joi.number(),
    bonus003: Joi.number(),
    bonus004: Joi.number(),
    movimentsRemains: Joi.number(),
    weaponsRemains: Joi.number()
  }).unknown(true);


  

module.exports = {
    getForm,
    postForm,
    postFormSchema
};